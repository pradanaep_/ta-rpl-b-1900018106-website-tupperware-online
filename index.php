<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Tupperware</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>

<body> 
    <nav>
        <label class="logo">Tupperware</label> 
        <ul>
            <li><a class="active" href="index.php">Home</a></li>
            <li><a href="#">produk <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="produk.php">Produk</a></li>
                    <li><a href="info.php">Info Produk</a></li>
                </ul> 
            </li>
            <li><a href="#">Login <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="admin/">Admin</a></li>
                    <?php if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])): ?>
                        <li><a href="login.php">Login</a></li>
                        <li><a href="daftar.php">Daftar</a></li>
                    <?php else: ?>
                        <li><a href="akun-saya.php">Akun Saya</a></li>
                        <li><a href="logout.php">Logout</a></li>
                    <?php endif ?>
                </ul>
            </li>
        </ul>
    </nav>
    <section>
    </section>
</body>
</html>