<?php

    require 'conn.php';
    require 'rupiah.php';
    session_start();

    if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }

    if (empty($_GET['id_produk'])) {
        echo "<script>alert('Silahkan produk dulu!'); window.location = 'produk.php'</script>";
    }else{
        $tampilProduk = $pdo->query("SELECT * FROM produk WHERE id_produk='$_GET[id_produk]'");
        $rProduk = $tampilProduk->fetch(PDO::FETCH_ASSOC);

        $tampilTransaksi = $pdo->query("SELECT * FROM transaksi WHERE id_transaksi='$_GET[id_transaksi]'");
        $rTransaksi = $tampilTransaksi->fetch(PDO::FETCH_ASSOC);
    }

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Detail Checkout | Tupperware</title>
    <link rel="stylesheet" href="admin/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>

<body>  
    <nav>
        <label class="logo">Tupperware</label> 
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a class="active" href="#">produk <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a class="active" href="produk.php">Produk</a></li>
                    <li><a href="info.php">Info Produk</a></li>
                </ul> 
            </li>
            <li><a href="#">Login <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="admin/">Admin</a></li>
                    <?php if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])): ?>
                        <li><a href="login.php">Login</a></li>
                        <li><a href="daftar.php">Daftar</a></li>
                    <?php else: ?>
                        <li><a href="akun-saya.php">Akun Saya</a></li>
                        <li><a href="logout.php">Logout</a></li>
                    <?php endif ?>
                </ul>
            </li>
        </ul>
    </nav>
    <section>
        <div class="wrapper">

            <div class="form">
                <h1 style="text-align: center;color: #23dbdb;">DETAIL CHECKOUT</h1>
                <h2 style="text-align: center;color: #28a745;">Status: "SUKSES"</h2>
                <br />
                <h4 style="text-align: center;">PRODUK YANG DI BELI</h4>
                <div class="foto">
                    <img src="img/<?= $rProduk['gambar']; ?>" alt="<?= $rProduk['nama_produk']; ?>">
                    <h1><?= $rProduk['nama_produk']; ?></h1>
                    <p>Rp<?= rp($rProduk['harga']); ?></p><br>
                </div>
                <br />
                <form method="POST" action="" enctype="multipart/form-data" style="text-align: center;">
                    <div class="mb-3">
                        <label for="qty" class="form-label">Qty</label>
                        <input type="number" class="form-control" id="qty" name="qty" value="<?= $rTransaksi['qty']; ?>" readonly>
                    </div>
                    <div class="mb-3">
                        <label for="total_bayar" class="form-label">Total Bayar</label>
                        <input type="text" class="form-control" id="total_bayar" value="Rp<?= rp($rTransaksi['total_bayar']); ?>" readonly>
                    </div>
                    <div class="mb-3">
                        <label for="bukti_transaksi" class="form-label">Bukti Transaksi</label>
                        <img src="img/transaksi/<?= $rTransaksi['bukti_transaksi']; ?>" alt="Bukti Transaksi <?= $rTransaksi['bukti_transaksi']; ?>" style="width: 100%;">
                    </div>
                    <br />
                    <a href="akun-saya.php" role="button" class="btn btn-primary">SELESAI <i class="fas fa-check-double"></i></a>
                </form>
            </div>

        </div>
    </section>
</body>
</html>