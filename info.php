<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Info Produk Tupperware</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>

<body> 
    <nav>
        <label class="logo">Tupperware</label> 
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a class="active" href="#">produk <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="produk.php">Produk</a></li>
                    <li><a class="active" href="info.php">Info Produk</a></li>
                </ul> 
            </li>
            <li><a href="#">Login <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="admin/">Admin</a></li>
                    <?php if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])): ?>
                        <li><a href="login.php">Login</a></li>
                        <li><a href="daftar.php">Daftar</a></li>
                    <?php else: ?>
                        <li><a href="akun-saya.php">Akun Saya</a></li>
                        <li><a href="logout.php">Logout</a></li>
                    <?php endif ?>
                </ul>
            </li>
        </ul>
    </nav>

    <section>
        <div class="gambarr">
            <div class="fotoo">
                <img src="img/info3.PNG">
            </div>

            <div class="fotoo">
                <img src="img/info2.PNG">
            </div>
        </div>
    </section>
</body>
</html>
