<?php
    require 'conn.php';
    require 'rupiah.php';  

    session_start(); 
 
    if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Akun Saya | Tupperware</title>
    <link rel="stylesheet" href="admin/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>

<body> 
    <nav>
        <label class="logo">Tupperware</label> 
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">produk <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="produk.php">Produk</a></li>
                    <li><a href="info.php">Info Produk</a></li>
                </ul> 
            </li>
            <li><a class="active" href="#">Login <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="admin/">Admin</a></li>
                    <?php if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])): ?>
                        <li><a href="login.php">Login</a></li>
                        <li><a href="daftar.php">Daftar</a></li>
                    <?php else: ?>
                        <li><a class="active" href="akun-saya.php">Akun Saya</a></li>
                        <li><a href="logout.php">Logout</a></li>
                    <?php endif ?>
                </ul>
            </li>
        </ul>
    </nav>
    <section>
        <div class="wrapper">
            <div class="form">
                <table class="table table-striped" style="color: #fff;text-align: center;">
                    <thead>
                        <tr>
                            <th colspan="4" style="color: #23dbdb;font-size: 24px;">DETAIL AKUN</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th scope="col">ID User</th>
                            <th scope="col">Nama User</th>
                            <th scope="col">Alamat Email</th>
                            <th scope="col">Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $tampilUser = $pdo->query("SELECT * FROM user WHERE id_user='$_SESSION[id_user]'");

                            $rUser = $tampilUser->fetch(PDO::FETCH_ASSOC);
                        ?>
                        <tr>
                            <td><?= $rUser['id_user']; ?></td>
                            <td><?= $rUser['nama_user']; ?></td>
                            <td><?= $rUser['email']; ?></td>
                            <td><?= $rUser['password']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form">
                <table class="table table-striped" style="color: #fff;text-align: center;">
                    <thead>
                        <tr>
                            <th colspan="8" style="color: #23dbdb;font-size: 24px;">RIWAYAT TRANSAKSI SAYA</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th scope="col">ID Transaksi</th>
                            <th scope="col">ID Produk</th>
                            <th scope="col">Nama Produk</th>
                            <th scope="col">Harga Produk</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Total Bayar</th>
                            <th scope="col">Waktu</th>
                            <th scope="col">Bukti Transaksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $tampilTransaksi = $pdo->query("SELECT 
                                                        produk.id_produk, produk.nama_produk, produk.harga, transaksi.qty, transaksi.id_transaksi, transaksi.total_bayar, transaksi.bukti_transaksi, transaksi.tanggal_transaksi
                                                        FROM transaksi
                                                        INNER JOIN produk ON transaksi.id_produk = produk.id_produk
                                                        WHERE transaksi.id_user='$_SESSION[id_user]'");

                            while ($rTransaksi = $tampilTransaksi->fetch(PDO::FETCH_ASSOC)) {
                        ?>
                        <tr>
                            <td><?= $rTransaksi['id_transaksi']; ?></td>
                            <td><?= $rTransaksi['id_produk']; ?></td>
                            <td><?= $rTransaksi['nama_produk']; ?></td>
                            <td>Rp<?= rp($rTransaksi['harga']); ?></td>
                            <td><?= $rTransaksi['qty']; ?></td>
                            <td>Rp<?= rp($rTransaksi['total_bayar']); ?></td>
                            <td><?= $rTransaksi['tanggal_transaksi']; ?></td>
                            <td><img src="img/transaksi/<?= $rTransaksi['bukti_transaksi']; ?>" alt="Gambar BUkti Transaksi <?= $rTransaksi['bukti_transaksi']; ?>" style="width: 200px;"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</body>
</html>